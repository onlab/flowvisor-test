#!/usr/bin/env python
'''Setuptools params'''

from setuptools import setup, find_packages

modname = distname = 'fvtest'

setup(
    name='fvtest',
    version='0.0.2',
    description='A FlowVisor Testing Framework',
    author='Dan Talayco/Tatsuya Yabe',
    author_email='tyabe@stanford.edu',
    packages=find_packages(),
    long_description="""\
FlowVisor test framework package.
      """,
      classifiers=[
          "License :: OSI Approved :: GNU General Public License (GPL)",
          "Programming Language :: Python",
          "Development Status :: 4 - Beta",
          "Intended Audience :: Developers",
          "Topic :: Internet",
      ],
      keywords='networking protocol Internet OpenFlow validation',
      license='unspecified',
      install_requires=[
        'setuptools',
        'doxypy',
        'pylint'
      ])
