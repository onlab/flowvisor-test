# If you chose to 'make' FVTest without sudo privilege with
#   $ make all
# you need to source this file before you run the test from
# tests directory as following:
#   $ source ../scripts/env.sh

export PYTHONPATH="../src/python"
