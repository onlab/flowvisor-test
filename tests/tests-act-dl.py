"""
Set_dl_dst/src action tests
"""

import sys
import logging
import templatetest
import testutils
import fvtest.cstruct as ofp
import fvtest.message as message
import fvtest.action as action
import fvtest.error as error
import fvtest.parse as parse

# ------ Start: Mandatory portion on each test case file ------

#@var basic_port_map Local copy of the configuration map from OF port
# numbers to OS interfaces
basic_port_map = None
#@var basic_logger Local logger object
basic_logger = None
#@var basic_timeout Local copy of global timeout value
basic_timeout = None
#@var basic_config Local copy of global configuration data
basic_config = None

test_prio = {}

def test_set_init(config):
    """
    Set up function for basic test classes
    @param config The configuration dictionary; see fvt
    """
    global basic_port_map
    global basic_fv_cmd
    global basic_logger
    global basic_timeout
    global basic_config

    basic_fv_cmd = config["fv_cmd"]
    basic_logger = logging.getLogger("actdl")
    basic_logger.info("Initializing test set")
    basic_timeout = config["timeout"]
    basic_port_map = config["port_map"]
    basic_config = config

# ------ End: Mandatory portion on each test case file ------

DST_MAC_FOR_CTL0_0 = "00:11:22:33:44:55"
DST_MAC_FOR_CTL0_1 = "00:55:44:33:22:11"
MAC_FOR_NOWHERE = "00:01:23:45:67:89"

def _genFlowModDl(parent,
                  orig_dl_src=testutils.SRC_MAC_FOR_CTL0_0,
                  set_dl_src=testutils.SRC_MAC_FOR_CTL0_0,
                  orig_dl_dst=DST_MAC_FOR_CTL0_0,
                  set_dl_dst=DST_MAC_FOR_CTL0_0,
                  out_ports=[1]):
    """
    Create flow_mod with specific dl_addr_match field and set_dl_addr action field
    Other parameters are chosen at random
    """
    pkt = testutils.simplePacket(pktlen=64, dl_src=orig_dl_src, dl_dst=orig_dl_dst)
    action_list = []
    if orig_dl_src != set_dl_src:
        actdl = action.action_set_dl_src()
        actdl.dl_addr = parse.parse_mac(set_dl_src)
        action_list.append(actdl)
    if orig_dl_dst != set_dl_dst:
        actdl = action.action_set_dl_dst()
        actdl.dl_addr = parse.parse_mac(set_dl_dst)
        action_list.append(actdl)
    for port in out_ports:
        act = action.action_output()
        act.port = port
        action_list.append(act)
    flow_mod = testutils.genFloModFromPkt(parent, pkt, ing_port=0, action_list=action_list)

    return flow_mod


class SetDlSrc1(templatetest.TemplateTest):
    """
    Check if FV accepts the command when original and set dl_src are in the slice
    """
    def setUp(self):
        templatetest.TemplateTest.setUp(self)
        self.logger = basic_logger
        # Prepare additional rules to set
        rule1 = ["changeFlowSpace", "ADD", "33000", "all", "dl_dst="+DST_MAC_FOR_CTL0_0, "Slice:controller0=4"]
        rule2 = ["changeFlowSpace", "ADD", "33000", "all", "dl_dst="+DST_MAC_FOR_CTL0_1, "Slice:controller0=4"]
        rule3 = ["listFlowSpace"]
        rules = [rule1, rule2, rule3]
        # Set up the test environment
        # -- Note: default setting: config_file = test-base.xml, num of SW = 1, num of CTL = 2
        (self.fv, self.sv, sv_ret, ctl_ret, sw_ret) = testutils.setUpTestEnv(self, fv_cmd=basic_fv_cmd, rules=rules)
        self.chkSetUpCondition(self.fv, sv_ret, ctl_ret, sw_ret)

    def runTest(self):
        flow_mod = _genFlowModDl(self,
                                 set_dl_src=testutils.SRC_MAC_FOR_CTL0_1,
                                 orig_dl_dst=MAC_FOR_NOWHERE,
                                 set_dl_dst=MAC_FOR_NOWHERE)

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetDlSrc2(SetDlSrc1):
    """
    Check if FV doesn't accept the command if original dl_src val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModDl(self,
                                 orig_dl_src=testutils.SRC_MAC_FOR_CTL1_0,
                                 set_dl_src=testutils.SRC_MAC_FOR_CTL1_1,
                                 orig_dl_dst=MAC_FOR_NOWHERE,
                                 set_dl_dst=MAC_FOR_NOWHERE)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["controller", 0, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetDlDst1(SetDlSrc1):
    """
    Check if FV accepts the command when original and set dl_dst are in the slice
    """
    def runTest(self):
        flow_mod = _genFlowModDl(self,
                                 orig_dl_src=MAC_FOR_NOWHERE,
                                 set_dl_src=MAC_FOR_NOWHERE,
                                 set_dl_dst=DST_MAC_FOR_CTL0_1)

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetDlDst2(SetDlSrc1):
    """
    Check if FV doesn't accept the command if original dl_dst val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModDl(self,
                                 orig_dl_src=MAC_FOR_NOWHERE,
                                 set_dl_src=MAC_FOR_NOWHERE,
                                 orig_dl_dst=testutils.SRC_MAC_FOR_CTL1_0,
                                 set_dl_dst=DST_MAC_FOR_CTL0_1)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["controller", 0, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))
