"""
Set_nw_dst/src action tests
"""

import sys
import logging
import templatetest
import testutils
import fvtest.cstruct as ofp
import fvtest.message as message
import fvtest.action as action
import fvtest.error as error
import fvtest.parse as parse

# ------ Start: Mandatory portion on each test case file ------

#@var basic_port_map Local copy of the configuration map from OF port
# numbers to OS interfaces
basic_port_map = None
#@var basic_logger Local logger object
basic_logger = None
#@var basic_timeout Local copy of global timeout value
basic_timeout = None
#@var basic_config Local copy of global configuration data
basic_config = None

test_prio = {}

def test_set_init(config):
    """
    Set up function for basic test classes
    @param config The configuration dictionary; see fvt
    """
    global basic_port_map
    global basic_fv_cmd
    global basic_logger
    global basic_timeout
    global basic_config

    basic_fv_cmd = config["fv_cmd"]
    basic_logger = logging.getLogger("actnw")
    basic_logger.info("Initializing test set")
    basic_timeout = config["timeout"]
    basic_port_map = config["port_map"]
    basic_config = config

# ------ End: Mandatory portion on each test case file ------

SRC_NW_FOR_CTL0_0 = '192.168.10.1'
SRC_NW_FOR_CTL0_1 = '192.168.10.2'
SRC_NW_FOR_CTL1_0 = '192.168.11.3'
SRC_NW_FOR_CTL1_1 = '192.168.11.4'
DST_NW_FOR_CTL0_0 = '192.168.12.5'
DST_NW_FOR_CTL0_1 = '192.168.12.6'
DST_NW_FOR_CTL1_0 = '192.168.13.7'
DST_NW_FOR_CTL1_1 = '192.168.13.8'
NW_FOR_NOWHERE = '192.168.254.254'


def _genFlowModNw(parent,
                  orig_nw_src=SRC_NW_FOR_CTL0_0,
                  set_nw_src=SRC_NW_FOR_CTL0_0,
                  orig_nw_dst=DST_NW_FOR_CTL0_0,
                  set_nw_dst=DST_NW_FOR_CTL0_0,
                  orig_nw_tos=0,
                  set_nw_tos=0,
                  out_ports=[1]):
    """
    Create flow_mod with specific nw_addr_match field and set_nw_addr action field
    Other parameters are chosen at random
    """
    pkt = testutils.simplePacket(pktlen=64, nw_src=orig_nw_src, nw_dst=orig_nw_dst, nw_tos=orig_nw_tos)
    action_list = []
    if orig_nw_src != set_nw_src:
        actnw = action.action_set_nw_src()
        actnw.nw_addr = parse.parse_ip(set_nw_src)
        action_list.append(actnw)
    if orig_nw_dst != set_nw_dst:
        actnw = action.action_set_nw_dst()
        actnw.nw_addr = parse.parse_ip(set_nw_dst)
        action_list.append(actnw)
    for port in out_ports:
        act = action.action_output()
        act.port = port
        action_list.append(act)
    flow_mod = testutils.genFloModFromPkt(parent, pkt, ing_port=0, action_list=action_list)

    return flow_mod


class SetNwSrc1(templatetest.TemplateTest):
    """
    Check if FV accepts the command when original and set nw_src are in the slice
    """
    def setUp(self):
        templatetest.TemplateTest.setUp(self)
        self.logger = basic_logger
        # Prepare additional rules to set
        rule1 = ["changeFlowSpace", "ADD", "33000", "all", "nw_src=192.168.10.0/24", "Slice:controller0=4"]
        rule2 = ["changeFlowSpace", "ADD", "33000", "all", "nw_dst=192.168.12.0/24", "Slice:controller0=4"]
        rule3 = ["changeFlowSpace", "ADD", "33000", "all", "nw_tos=1", "Slice:controller0=4"]
        rule4 = ["changeFlowSpace", "ADD", "33000", "all", "nw_tos=2", "Slice:controller0=4"]
        rule5 = ["listFlowSpace"]
        rules = [rule1, rule2, rule3, rule4, rule5]
        # Set up the test environment
        # -- Note: default setting: config_file = test-base.xml, num of SW = 1, num of CTL = 2
        (self.fv, self.sv, sv_ret, ctl_ret, sw_ret) = testutils.setUpTestEnv(self, fv_cmd=basic_fv_cmd, rules=rules)
        self.chkSetUpCondition(self.fv, sv_ret, ctl_ret, sw_ret)

    def runTest(self):
        flow_mod = _genFlowModNw(self,
                                 set_nw_src=SRC_NW_FOR_CTL0_1,
                                 orig_nw_dst=NW_FOR_NOWHERE,
                                 set_nw_dst=NW_FOR_NOWHERE)

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetNwSrc2(SetNwSrc1):
    """
    Check if FV doesn't accept the command if original nw_src val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModNw(self,
                                 orig_nw_src=SRC_NW_FOR_CTL1_0,
                                 set_nw_src=SRC_NW_FOR_CTL1_1,
                                 orig_nw_dst=NW_FOR_NOWHERE,
                                 set_nw_dst=NW_FOR_NOWHERE)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["controller", 0, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetNwDst1(SetNwSrc1):
    """
    Check if FV accepts the command when original and set nw_dst are in the slice
    """
    def runTest(self):
        flow_mod = _genFlowModNw(self,
                                 orig_nw_src=NW_FOR_NOWHERE,
                                 set_nw_src=NW_FOR_NOWHERE,
                                 set_nw_dst=DST_NW_FOR_CTL0_1)

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetNwDst2(SetNwSrc1):
    """
    Check if FV doesn't accept the command if original nw_dst val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModNw(self,
                                 orig_nw_src=NW_FOR_NOWHERE,
                                 set_nw_src=NW_FOR_NOWHERE,
                                 orig_nw_dst=SRC_NW_FOR_CTL1_0,
                                 set_nw_dst=DST_NW_FOR_CTL0_1)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["controller", 0, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetNwTos1(SetNwSrc1):
    """
    Check if FV accepts the command when original and set nw_tos are in the slice
    """
    def runTest(self):
        flow_mod = _genFlowModNw(self,
                                 orig_nw_src=NW_FOR_NOWHERE,
                                 set_nw_src=NW_FOR_NOWHERE,
                                 orig_nw_dst=SRC_NW_FOR_CTL1_0,
                                 set_nw_dst=DST_NW_FOR_CTL1_0,
                                 orig_nw_tos=1,
                                 set_nw_tos=2)

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetNwTos2(SetNwSrc1):
    """
    Check if FV doesn't accept the command if original nw_tos val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModNw(self,
                                 orig_nw_src=NW_FOR_NOWHERE,
                                 set_nw_src=NW_FOR_NOWHERE,
                                 orig_nw_dst=SRC_NW_FOR_CTL1_0,
                                 set_nw_dst=DST_NW_FOR_CTL1_0,
                                 orig_nw_tos=3,
                                 set_nw_tos=2)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["controller", 0, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))
