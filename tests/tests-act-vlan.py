"""
Set_vlan/Strip_vlan action tests
"""

import sys
import logging
import templatetest
import testutils
import fvtest.cstruct as ofp
import fvtest.message as message
import fvtest.action as action
import fvtest.error as error

# ------ Start: Mandatory portion on each test case file ------

#@var basic_port_map Local copy of the configuration map from OF port
# numbers to OS interfaces
basic_port_map = None
#@var basic_logger Local logger object
basic_logger = None
#@var basic_timeout Local copy of global timeout value
basic_timeout = None
#@var basic_config Local copy of global configuration data
basic_config = None

test_prio = {}

def test_set_init(config):
    """
    Set up function for basic test classes
    @param config The configuration dictionary; see fvt
    """
    global basic_port_map
    global basic_fv_cmd
    global basic_logger
    global basic_timeout
    global basic_config

    basic_fv_cmd = config["fv_cmd"]
    basic_logger = logging.getLogger("actvlan")
    basic_logger.info("Initializing test set")
    basic_timeout = config["timeout"]
    basic_port_map = config["port_map"]
    basic_config = config

# ------ End: Mandatory portion on each test case file ------


def _genFlowModVlan(parent, orig_vlan=812, orig_pcp=0, set_vlan=813, set_pcp=0, strip_vlan=False, out_ports=[1]):
    """
    Create flow_mod with specific vlan_match field and vlan_action field
    Other parameters are chosen at random
    """
    pkt = testutils.simplePacket(pktlen=64, dl_src=testutils.SRC_MAC_FOR_CTL0_0, dl_vlan=orig_vlan)
    action_list = []
    if strip_vlan:
        actvlan = action.action_strip_vlan()
        action_list.append(actvlan)
    else:
        if orig_vlan != set_vlan:
            actvlan = action.action_set_vlan_vid()
            actvlan.vlan_vid = set_vlan
            action_list.append(actvlan)
        if orig_pcp != set_pcp:
            actvlan = action.action_set_vlan_pcp()
            actvlan.vlan_pcp = set_pcp
            action_list.append(actvlan)
    for port in out_ports:
        act = action.action_output()
        act.port = port
        action_list.append(act)
    flow_mod = testutils.genFloModFromPkt(parent, pkt, ing_port=0, action_list=action_list)

    return flow_mod


class SetVlan1(templatetest.TemplateTest):
    """
    Check if FV accepts the command when original and set VLAN are in the slice
    """
    def setUp(self):
        templatetest.TemplateTest.setUp(self)
        self.logger = basic_logger
        # Prepare additional rules to set
        rule1 = ["changeFlowSpace", "ADD", "33000", "all", "dl_vlan=812", "Slice:controller1=4"]
        rule2 = ["changeFlowSpace", "ADD", "33000", "all", "dl_vlan=813", "Slice:controller1=4"]
        rule3 = ["changeFlowSpace", "ADD", "33000", "all", "dl_vlan=815", "Slice:controller0=4"]
        rule4 = ["listFlowSpace"]
        rules = [rule1, rule2, rule3, rule4]
        # Set up the test environment
        # -- Note: default setting: config_file = test-base.xml, num of SW = 1, num of CTL = 2
        (self.fv, self.sv, sv_ret, ctl_ret, sw_ret) = testutils.setUpTestEnv(self, fv_cmd=basic_fv_cmd, rules=rules)
        self.chkSetUpCondition(self.fv, sv_ret, ctl_ret, sw_ret)

    def runTest(self):
        flow_mod = _genFlowModVlan(self)

        snd_list = ["controller", 1, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetVlan2(SetVlan1):
    """
    Check if FV doesn't accept the command if original VLAN val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModVlan(self, orig_vlan=815)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 1, 0, flow_mod]
        exp_list = [["controller", 1, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

'''
class SetVlan3(SetVlan1):
    """
    Check if it doesn't accept if set VLAN val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModVlan(self, set_vlan=815)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 1, 0, flow_mod]
        exp_list = [["controller", 1, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))
'''

class StripVlan1(SetVlan1):
    """
    Check if it accepts when original VLAN is in the slice
    """
    def runTest(self):
        flow_mod = _genFlowModVlan(self, strip_vlan=True)

        snd_list = ["controller", 1, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class StripVlan2(SetVlan1):
    """
    Check if it doesn't accept if original VLAN val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModVlan(self, orig_vlan=815, strip_vlan=True)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 1, 0, flow_mod]
        exp_list = [["controller", 1, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

'''
class SetVlanPcp1(SetVlan1):
    """
    Check if it accepts in whatever case
    """
    def runTest(self):
        flow_mod = _genFlowModVlan(self, orig_vlan=815, orig_pcp=4, set_vlan=815, set_pcp=5)

        snd_list = ["controller", 1, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))
'''
