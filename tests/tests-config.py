"""
Set_config and get_config exchange test
Check if FV maintains the biggest miss_send_len for the switch
Check if FV returns the original miss_send_len value that the
controller has set
"""

import sys
import logging
import templatetest
import testutils
import fvtest.cstruct as ofp
import fvtest.message as message
import fvtest.action as action
import fvtest.error as error

# ------ Start: Mandatory portion on each test case file ------

#@var basic_port_map Local copy of the configuration map from OF port
# numbers to OS interfaces
basic_port_map = None
#@var basic_logger Local logger object
basic_logger = None
#@var basic_timeout Local copy of global timeout value
basic_timeout = None
#@var basic_config Local copy of global configuration data
basic_config = None

test_prio = {}

def test_set_init(config):
    """
    Set up function for basic test classes
    @param config The configuration dictionary; see fvt
    """
    global basic_port_map
    global basic_fv_cmd
    global basic_logger
    global basic_timeout
    global basic_config

    basic_fv_cmd = config["fv_cmd"]
    basic_logger = logging.getLogger("config")
    basic_logger.info("Initializing test set")
    basic_timeout = config["timeout"]
    basic_port_map = config["port_map"]
    basic_config = config

# ------ End: Mandatory portion on each test case file ------
    global MISS_SEND_LEN
    global BIGGER_MISS_SEND_LEN
    global SMALLER_MISS_SEND_LEN
    MISS_SEND_LEN = 128
    BIGGER_MISS_SEND_LEN = 256
    SMALLER_MISS_SEND_LEN = 64

class SetGetConfig(templatetest.TemplateTest):
    """
    FV should send set_config with its biggest miss_send_len to a switch
    """
    def setUp(self):
        templatetest.TemplateTest.setUp(self)
        self.logger = basic_logger
        # Set up the test environment
        # -- Note: default setting: config_file = test-base.xml, num of SW = 1, num of CTL = 2
        (self.fv, self.sv, sv_ret, ctl_ret, sw_ret) = testutils.setUpTestEnv(self, fv_cmd=basic_fv_cmd)
        self.chkSetUpCondition(self.fv, sv_ret, ctl_ret, sw_ret)

    def runTest(self):

        # First setting

        # SetConfig
        msg_set = message.set_config()
        msg_set.miss_send_len = MISS_SEND_LEN

        snd_list = ["controller", 0, 0, msg_set]
        exp_list = [["switch", 0, msg_set]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))

        # GetConfigRequest
        msg_req = message.get_config_request()

        snd_list = ["controller", 0, 0, msg_req]
        exp_list = [["switch", 0, msg_req]]
        (res, ret_xid) = testutils.ofmsgSndCmpWithXid(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))
        return ret_xid

        # GetConfigReply
        msg_rep = message.get_config_reply()
        msg_rep.header.xid = ret_xid
        msg_rep.miss_send_len = msg_set.miss_send_len

        snd_list = ["switch", 0, msg_rep]
        exp_list = [["controller", 0, msg_rep]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))

        # Second setting
        # The value of miss_send_len is bigger than the first one.
        # FV uses the new bigger value

        # SetConfig
        msg_set_big = message.set_config()
        msg_set_big.miss_send_len = BIGGER_MISS_SEND_LEN

        snd_list = ["controller", 0, 0, msg_set_big]
        exp_list = [["switch", 0, msg_set_big]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))

        # GetConfigRequest
        # Use the already-creqted message
        snd_list = ["controller", 0, 0, msg_req]
        exp_list = [["switch", 0, msg_req]]
        (res, ret_xid) = testutils.ofmsgSndCmpWithXid(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))
        return ret_xid

        # GetConfigReply
        msg_rep = message.get_config_reply()
        msg_rep.header.xid = ret_xid
        msg_rep.miss_send_len = msg_set_big.miss_send_len

        snd_list = ["switch", 0, msg_rep]
        exp_list = [["controller", 0, msg_rep]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))

        # Third setting from DIFFERENT controller
        # This time the value of miss_send_len is smaller than the previous ones,
        # so FV keeps using the biggest of what FV has 

        # SetConfig
        msg_set_sml = message.set_config()
        msg_set_sml.miss_send_len = SMALLER_MISS_SEND_LEN

        snd_list = ["controller", 1, 0, msg_set_sml]
        exp_list = [["switch", 0, msg_set_big]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))

        # GetConfigRequest
        # Use the already-creqted message
        snd_list = ["controller", 1, 0, msg_req]
        exp_list = [["switch", 0, msg_req]]
        (res, ret_xid) = testutils.ofmsgSndCmpWithXid(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))

        # GetConfigReply
        # FV should send back the original value to the controller
        msg_rep_sw = message.get_config_reply()
        msg_rep_sw.header.xid = ret_xid
        msg_rep_sw.miss_send_len = msg_set_big.miss_send_len

        msg_rep_ctl = message.get_config_reply()
        msg_rep_ctl.header.xid = ret_xid
        msg_rep_ctl.miss_send_len = msg_set_sml.miss_send_len

        snd_list = ["switch", 0, msg_rep_sw]
        exp_list = [["controller", 1, msg_rep_ctl]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Req: Received unexpected message" %(self.__class__.__name__))
