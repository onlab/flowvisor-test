"""
Set_tp_dst/src action tests
"""

import sys
import logging
import templatetest
import testutils
import fvtest.cstruct as ofp
import fvtest.message as message
import fvtest.action as action
import fvtest.error as error
import fvtest.parse as parse

# ------ Start: Mandatory portion on each test case file ------

#@var basic_port_map Local copy of the configuration map from OF port
# numbers to OS interfaces
basic_port_map = None
#@var basic_logger Local logger object
basic_logger = None
#@var basic_timeout Local copy of global timeout value
basic_timeout = None
#@var basic_config Local copy of global configuration data
basic_config = None

test_prio = {}

def test_set_init(config):
    """
    Set up function for basic test classes
    @param config The configuration dictionary; see fvt
    """
    global basic_port_map
    global basic_fv_cmd
    global basic_logger
    global basic_timeout
    global basic_config

    basic_fv_cmd = config["fv_cmd"]
    basic_logger = logging.getLogger("acttp")
    basic_logger.info("Initializing test set")
    basic_timeout = config["timeout"]
    basic_port_map = config["port_map"]
    basic_config = config

# ------ End: Mandatory portion on each test case file ------

SRC_TP_FOR_CTL0_0 = 11000
SRC_TP_FOR_CTL0_1 = 12000
SRC_TP_FOR_CTL1_0 = 13000
SRC_TP_FOR_CTL1_1 = 14000
DST_TP_FOR_CTL0_0 = 15000
DST_TP_FOR_CTL0_1 = 16000
DST_TP_FOR_CTL1_0 = 17000
DST_TP_FOR_CTL1_1 = 18000
TP_FOR_NOWHERE = 20000


def _genFlowModTp(parent,
                  orig_tp_src=SRC_TP_FOR_CTL0_0,
                  set_tp_src=SRC_TP_FOR_CTL0_0,
                  orig_tp_dst=DST_TP_FOR_CTL0_0,
                  set_tp_dst=DST_TP_FOR_CTL0_0,
                  out_ports=[1]):
    """
    Create flow_mod with specific tp_port_match field and set_tp_port action field
    Other parameters are chosen at random
    """
    pkt = testutils.simplePacket(pktlen=64, tp_src=orig_tp_src, tp_dst=orig_tp_dst)
    action_list = []
    if orig_tp_src != set_tp_src:
        actnw = action.action_set_tp_src()
        actnw.nw_addr = set_tp_src
        action_list.append(actnw)
    if orig_tp_dst != set_tp_dst:
        actnw = action.action_set_tp_dst()
        actnw.nw_addr = set_tp_dst
        action_list.append(actnw)
    for port in out_ports:
        act = action.action_output()
        act.port = port
        action_list.append(act)
    flow_mod = testutils.genFloModFromPkt(parent, pkt, ing_port=0, action_list=action_list)

    return flow_mod


class SetTpSrc1(templatetest.TemplateTest):
    """
    Check if FV accepts the command when original and set tp_src are in the slice
    """
    def setUp(self):
        templatetest.TemplateTest.setUp(self)
        self.logger = basic_logger
        # Prepare additional rules to set
        rule1 = ["changeFlowSpace", "ADD", "33000", "all", "tp_src=11000", "Slice:controller0=4"]
        rule2 = ["changeFlowSpace", "ADD", "33000", "all", "tp_src=12000", "Slice:controller0=4"]
        rule3 = ["changeFlowSpace", "ADD", "33000", "all", "tp_dst=14000", "Slice:controller0=4"]
        rule4 = ["changeFlowSpace", "ADD", "33000", "all", "tp_dst=15000", "Slice:controller0=4"]
        rule5 = ["listFlowSpace"]
        rules = [rule1, rule2, rule3, rule4, rule5]
        # Set up the test environment
        # -- Note: default setting: config_file = test-base.xml, num of SW = 1, num of CTL = 2
        (self.fv, self.sv, sv_ret, ctl_ret, sw_ret) = testutils.setUpTestEnv(self, fv_cmd=basic_fv_cmd, rules=rules)
        self.chkSetUpCondition(self.fv, sv_ret, ctl_ret, sw_ret)

    def runTest(self):
        flow_mod = _genFlowModTp(self,
                                 set_tp_src=SRC_TP_FOR_CTL0_1,
                                 orig_tp_dst=TP_FOR_NOWHERE,
                                 set_tp_dst=TP_FOR_NOWHERE)

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetTpSrc2(SetTpSrc1):
    """
    Check if FV doesn't accept the command if original tp_src val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModTp(self,
                                 orig_tp_src=SRC_TP_FOR_CTL1_0,
                                 set_tp_src=SRC_TP_FOR_CTL1_1,
                                 orig_tp_dst=TP_FOR_NOWHERE,
                                 set_tp_dst=TP_FOR_NOWHERE)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["controller", 0, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetTpDst1(SetTpSrc1):
    """
    Check if FV accepts the command when original and set tp_dst are in the slice
    """
    def runTest(self):
        flow_mod = _genFlowModTp(self,
                                 orig_tp_src=TP_FOR_NOWHERE,
                                 set_tp_src=TP_FOR_NOWHERE,
                                 set_tp_dst=DST_TP_FOR_CTL0_1)

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["switch", 0, flow_mod]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class SetTpDst2(SetTpSrc1):
    """
    Check if FV doesn't accept the command if original tp_dst val is out of slice
    """
    def runTest(self):
        flow_mod = _genFlowModTp(self,
                                 orig_tp_src=TP_FOR_NOWHERE,
                                 set_tp_src=TP_FOR_NOWHERE,
                                 orig_tp_dst=SRC_TP_FOR_CTL1_0,
                                 set_tp_dst=DST_TP_FOR_CTL0_1)

        err = error.flow_mod_failed_error_msg()
        err.code = ofp.OFPFMFC_EPERM
        err.data = flow_mod.pack()

        snd_list = ["controller", 0, 0, flow_mod]
        exp_list = [["controller", 0, err]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, hdr_only=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))
