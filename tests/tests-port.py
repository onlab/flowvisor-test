"""
Port status checking and port adding, changing and deleting
"""

import sys
import logging
import templatetest
import testutils
import fvtest.cstruct as ofp
import fvtest.message as message
import fvtest.action as action

# ------ Start: Mandatory portion on each test case file ------

#@var basic_port_map Local copy of the configuration map from OF port
# numbers to OS interfaces
basic_port_map = None
#@var basic_logger Local logger object
basic_logger = None
#@var basic_timeout Local copy of global timeout value
basic_timeout = None
#@var basic_config Local copy of global configuration data
basic_config = None

test_prio = {}

def test_set_init(config):
    """
    Set up function for basic test classes
    @param config The configuration dictionary; see fvt
    """
    global basic_port_map
    global basic_fv_cmd
    global basic_logger
    global basic_timeout
    global basic_config

    basic_fv_cmd = config["fv_cmd"]
    basic_logger = logging.getLogger("port")
    basic_logger.info("Initializing test set")
    basic_timeout = config["timeout"]
    basic_port_map = config["port_map"]
    basic_config = config

# ------ End: Mandatory portion on each test case file ------

class PortStats(templatetest.TemplateTest):
    """
    Port_stats_request
    Check if switch gets the same port_stats message as controller has
    issued
    Check if controller gets the response only about the ports it has
    """
    def setUp(self):
        templatetest.TemplateTest.setUp(self)
        self.logger = basic_logger
        # Prepare additional rules to set
        # Set a rule in addition to a default config,
        # Now controller0 handles packets with specific dl_src from/to any ports
        rule1 = ["changeFlowSpace", "ADD", "34000", "all", "dl_src=00:11:22:33:44:55", "Slice:controller0=4"]
        rule2 = ["listFlowSpace"]
        rules = [rule1, rule2]
        # Set up the test environment
        # -- Note: default setting: config_file = test-base.xml, num of SW = 1, num of CTL = 2
        (self.fv, self.sv, sv_ret, ctl_ret, sw_ret) = testutils.setUpTestEnv(self, fv_cmd=basic_fv_cmd, rules=rules)
        self.chkSetUpCondition(self.fv, sv_ret, ctl_ret, sw_ret)

    def runTest(self):
        msg = message.port_stats_request()
        msg.header.xid = testutils.genVal32bit()
        msg.port_no = ofp.OFPP_NONE

        rep0 = message.port_stats_entry()
        rep0.port_no = 0
        rep1 = message.port_stats_entry()
        rep1.port_no = 1
        rep2 = message.port_stats_entry()
        rep2.port_no = 2
        rep3 = message.port_stats_entry()
        rep3.port_no = 3

        # Ctl0 : has all the switch ports

        # Port Stats Request
        snd_list = ["controller", 0, 0, msg]
        exp_list = [["switch", 0, msg]]
        (res, ret_xid) = testutils.ofmsgSndCmpWithXid(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

        # Port Stats Reply
        rep_msg = message.port_stats_reply()
        rep_msg.header.xid = ret_xid
        rep_msg.stats = [rep0, rep1, rep2, rep3]

        snd_list = ["switch", 0, rep_msg]
        exp_list = [["controller", 0, rep_msg]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

        # Ctl1: has some of the switch ports

        # Port Stats Request
        snd_list = ["controller", 1, 0, msg]
        exp_list = [["switch", 0, msg]]
        (res, ret_xid) = testutils.ofmsgSndCmpWithXid(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

        # Port Stats Reply
        rep_msg_sw = message.port_stats_reply()
        rep_msg_sw.header.xid = ret_xid
        rep_msg_sw.stats = [rep0, rep1, rep2, rep3]
        rep_msg_ctl = message.port_stats_reply()
        rep_msg_ctl.header.xid = ret_xid
        rep_msg_ctl.stats = [rep1, rep3]

        snd_list = ["switch", 0, rep_msg_sw]
        exp_list = [["controller", 1, rep_msg_ctl]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

'''
class QueueStats(PortStats):
    """
    Queue_stats_request and reply
    Check if switch gets the same queue_stats message as controller has
    issued
    Check if controller gets the response only about the ports it has
    """
    def runTest(self):
        msg = message.queue_stats_request()
        msg.header.xid = testutils.genVal32bit()
        msg.port_no = ofp.OFPP_NONE

        rep0 = message.queue_stats_entry()
        rep0.port_no = 0
        rep1 = message.queue_stats_entry()
        rep1.port_no = 1
        rep2 = message.queue_stats_entry()
        rep2.port_no = 2
        rep3 = message.queue_stats_entry()
        rep3.port_no = 3

        # Ctl0 : has all the switch ports

        # Queue Stats Request
        snd_list = ["controller", 0, 0, msg]
        exp_list = [["switch", 0, msg]]
        (res, ret_xid) = testutils.ofmsgSndCmpWithXid(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

        # Queue Stats Reply
        rep_msg = message.queue_stats_reply()
        rep_msg.header.xid = ret_xid
        rep_msg.stats = [rep0, rep1, rep2, rep3]

        snd_list = ["switch", 0, rep_msg]
        exp_list = [["controller", 0, rep_msg]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

        # Ctl1: has some of the switch ports

        # Queue Stats Request
        snd_list = ["controller", 1, 0, msg]
        exp_list = [["switch", 0, msg]]
        (res, ret_xid) = testutils.ofmsgSndCmpWithXid(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

        # Queue Stats Reply
        rep_msg_sw = message.queue_stats_reply()
        rep_msg_sw.header.xid = ret_xid
        rep_msg_sw.stats = [rep0, rep1, rep2, rep3]
        rep_msg_ctl = message.queue_stats_reply()
        rep_msg_ctl.header.xid = ret_xid
        rep_msg_ctl.stats = [rep1, rep3]

        snd_list = ["switch", 0, rep_msg_sw]
        exp_list = [["controller", 1, rep_msg_ctl]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))
'''

class PktOutInport(PortStats):
    """
    Packet_out to flood port with ingress port mentioned
    Check if FlowVisor expands the message so that it can exclude ingress_port
    from the message when it sends it to switch
    """
    def runTest(self):
        ports_ctl = [ofp.OFPP_FLOOD]
        pkt_ctl = testutils.simplePacket(dl_src="00:11:22:33:44:55")
        pktout_ctl = testutils.genPacketOut(self, in_port=1, action_ports=ports_ctl, pkt=pkt_ctl)

        ports_sw = [0,2,3] # Expect port1 to be excluded
        pktout_sw = testutils.genPacketOut(self, in_port=1, action_ports=ports_sw, pkt=pkt_ctl)

        snd_list = ["controller", 0, 0, pktout_ctl]
        exp_list = [["switch", 0, pktout_sw]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))


class AddModDelPort(PortStats):
    """
    Port_status with adding a port, changing it then removing it
    When switch sends port_status with 'add port',
    check if controller which has all the switch ports can receive it
    while controller which has only specific ports cannot
    Then send 'modify port' from switch and check it is seen on controller
    Finally send 'remove port' from switch and check if the controller handles it
    """
    def runTest(self):
        # Controller0 should be able to see the status change
        # but controller1 should not
        port13 = testutils.genPhyPort("port13", [00,0x13,0x44,0x66,0x88,0xaa], 13)
        msg = message.port_status()
        msg.reason = ofp.OFPPR_ADD
        msg.desc = port13

        snd_list = ["switch", 0, msg]
        exp_list = [["controller", 0, msg]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Status_Add: Received unexpected message" %(self.__class__.__name__))

        # Ctl0 to sw packet_out_flood. Added port should be visible
        ports_ctl = [ofp.OFPP_FLOOD]
        pkt_ctl = testutils.simplePacket(dl_src="00:11:22:33:44:55")
        pktout_ctl = testutils.genPacketOut(self, action_ports=ports_ctl, pkt=pkt_ctl)

        ports_sw = [0,1,2,3,13]
        pktout_sw = testutils.genPacketOut(self, action_ports=ports_sw, pkt=pkt_ctl)

        snd_list = ["controller", 0, 0, pktout_ctl]
        exp_list = [["switch", 0, pktout_sw]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Status_Add: PktOut: Received unexpected message" %(self.__class__.__name__))

        # Ctl1 to sw packet_out_flood. Added port shouldn't be visible
        ports_ctl = [ofp.OFPP_FLOOD]
        pkt_ctl = testutils.simplePacket(dl_src=testutils.SRC_MAC_FOR_CTL1_0)
        pktout_ctl = testutils.genPacketOut(self, action_ports=ports_ctl, pkt=pkt_ctl)

        ports_sw = [1,3]
        pktout_sw = testutils.genPacketOut(self, action_ports=ports_sw, pkt=pkt_ctl)

        # Test
        snd_list = ["controller", 1, 0, pktout_ctl]
        exp_list = [["switch", 0, pktout_sw]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Status_Add: PktOut: Received unexpected message" %(self.__class__.__name__))

        # Modify the port
        port13.name = "port13_mod"
        msg = message.port_status()
        msg.reason = ofp.OFPPR_MODIFY
        msg.desc = port13

        snd_list = ["switch", 0, msg]
        exp_list = [["controller", 0, msg]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Status_Mod: Received unexpected message" %(self.__class__.__name__))

        # Check if added port still works
        ports_ctl = [ofp.OFPP_FLOOD]
        pkt_ctl = testutils.simplePacket(dl_src="00:11:22:33:44:55")
        pktout_ctl = testutils.genPacketOut(self, action_ports=ports_ctl, pkt=pkt_ctl)

        ports_sw = [0,1,2,3,13]
        pktout_sw = testutils.genPacketOut(self, action_ports=ports_sw, pkt=pkt_ctl)

        snd_list = ["controller", 0, 0, pktout_ctl]
        exp_list = [["switch", 0, pktout_sw]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Status_Mod: PktOut: Received unexpected message" %(self.__class__.__name__))

        # Remove the added port
        msg = message.port_status()
        msg.reason = ofp.OFPPR_DELETE
        msg.desc = port13

        snd_list = ["switch", 0, msg]
        exp_list = [["controller", 0, msg]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Status_Del: Received unexpected message" %(self.__class__.__name__))

        # Check if deleted port now invisible
        ports_sw = [0,1,2,3]
        pktout_sw = testutils.genPacketOut(self, action_ports=ports_sw, pkt=pkt_ctl)

        snd_list = ["controller", 0, 0, pktout_ctl]
        exp_list = [["switch", 0, pktout_sw]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Status_Del: PktOut: Received unexpected message" %(self.__class__.__name__))


class RmPortApi(PortStats):
    """
    ChangeFlowSpace with Remove via API
    Remove a port from flow space using API
    Check if the removed port is now invisible
    """
    def runTest(self):
        ctl1_port1_ids = ["13","15"] # Related IDs in listFlowSpace
        for ctl1_id in ctl1_port1_ids:
            rule = ["changeFlowSpace", "REMOVE", ctl1_id]
            (success, data) = testutils.setRule(self, self.sv, rule)
            self.assertTrue(success, "%s: Not success" %(self.__class__.__name__))

        # Ctl1 to sw packet_out_flood. port1 now shouldn't be visible
        ports_ctl = [ofp.OFPP_FLOOD]
        pkt_ctl = testutils.simplePacket(dl_src=testutils.SRC_MAC_FOR_CTL1_0)
        pktout_ctl = testutils.genPacketOut(self, action_ports=ports_ctl, pkt=pkt_ctl)

        ports_sw = [3] # Expect port1 to be excluded
        pktout_sw = testutils.genPacketOut(self, action_ports=ports_sw, pkt=pkt_ctl)

        snd_list = ["controller", 1, 0, pktout_ctl]
        exp_list = [["switch", 0, pktout_sw]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: PacketOut: Received unexpected message" %(self.__class__.__name__))


class PortMod(PortStats):
    """
    Issue port_mod from ctl and see the behavior
    Issue port_mod with 'NO_FLOOD'. Check if sw receives it via FV
    Also check if the specified port is not included when flow_mod with
    flood has been issued
    """
    def runTest(self):
        port13_hw_addr = [00,0x13,0x44,0x66,0x88,0xaa]
        port13 = testutils.genPhyPort("port13", port13_hw_addr, 13)
        msg = message.port_status()
        msg.reason = ofp.OFPPR_ADD
        msg.desc = port13

        snd_list = ["switch", 0, msg]
        exp_list = [["controller", 0, msg]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Status_Add: Received unexpected message" %(self.__class__.__name__))

        # Sw to Ctl0 packet_in. Should be received
        pkt = testutils.simplePacket(dl_src = "00:11:22:33:44:55")
        in_port = 13 #CTL0 has this port
        msg = testutils.genPacketIn(in_port=in_port, pkt=pkt)

        snd_list = ["switch", 0, msg]
        exp_list = [["controller", 0, msg]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

        # Issue port_mod saying 'disable flood'
        msg = message.port_mod()
        msg.header.xid = testutils.genVal32bit()
        msg.port_no = 13
        msg.hw_addr = port13_hw_addr
        msg.config = (0 + ofp.OFPPC_PORT_DOWN
                       + ofp.OFPPC_NO_STP
                       + ofp.OFPPC_NO_RECV
                       + ofp.OFPPC_NO_RECV_STP
                       + ofp.OFPPC_NO_FLOOD
                       + ofp.OFPPC_NO_FWD
                       + ofp.OFPPC_NO_PACKET_IN)
        msg.mask = msg.config

        # This message will be consumed by FlowVisor
        snd_list = ["controller", 0, 0, msg]
        exp_list = [["switch", 0, None]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))

        '''
        ### Port_mod currently not working on FlowVisor

        # Sw to Ctl0 packet_in. Should NOT be received
        pkt = testutils.simplePacket(dl_src = "00:11:22:33:44:55")
        in_port = 13 #CTL0 has this port
        msg = testutils.genPacketIn(in_port=in_port, pkt=pkt)

        snd_list = ["switch", 0, msg]
        exp_list = [["controller", 0, None]]
        res = testutils.ofmsgSndCmp(self, snd_list, exp_list, xid_ignore=True)
        self.assertTrue(res, "%s: Received unexpected message" %(self.__class__.__name__))
        '''
